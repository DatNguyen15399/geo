package com.falcon.core.geo;

import com.maxmind.geoip2.DatabaseReader;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.InetAddress;

@Component
public class GeoResolver {
	private DatabaseReader databaseReader;

	@PostConstruct
	void init() throws IOException {
		ClassPathResource classPathResource = new ClassPathResource("db/GeoLite2-City.mmdb");
		databaseReader = new DatabaseReader.Builder(classPathResource.getInputStream()).build();
	}

	public GeoInfoResponse getLocation(String ip) {
		try {
			InetAddress inetAddress = InetAddress.getByName(ip);
			var res = databaseReader.city(inetAddress);

			GeoInfoResponse response = new GeoInfoResponse();
			response.setCountryName(res.getCountry().getName());
			response.setCountryCode(res.getCountry().getIsoCode());
			response.setCity(res.getCity().getName());
			response.setTimeZone(res.getLocation().getTimeZone());
			response.setLongitude(res.getLocation().getLongitude());
			response.setLatitude(res.getLocation().getLatitude());
			response.setPostalCode(res.getPostal().getCode());
			response.setAccuracy(res.getLocation().getAccuracyRadius());
			response.setMetroCode(res.getLocation().getMetroCode());
			return response;
		} catch (Exception e) {
			throw new ResolveIpException(e.getMessage());
		}
	}
}
