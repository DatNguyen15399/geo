package com.falcon.core.geo;

public class ResolveIpException extends RuntimeException {
	public ResolveIpException(String mes) {
		super(mes);
	}
}
